import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
//import { LandingPageModule } from './landing-page/landing-page.module';
import { StartModule } from './start/start.module';
import { View1Module } from './view1/view1.module';
import { View2Module } from './view2/view2.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    //LandingPageModule,
    StartModule,
    View1Module,
    View2Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
