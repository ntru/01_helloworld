import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyC1Component } from './c1/c1';
import { MyC2Component } from './c2/c2';

@NgModule({
  declarations: [
    MyC1Component,
    MyC2Component
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    MyC1Component,
    MyC2Component
  ]
})
export class MyComponents { }
