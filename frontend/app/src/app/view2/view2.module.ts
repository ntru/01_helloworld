import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { View2Component } from './view2.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRippleModule } from '@angular/material/core';
import { MyComponents } from '../components/MyComponents/my-components.module';

@NgModule({
  declarations: [View2Component],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatButtonModule,
    MyComponents
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatButtonModule,
    View2Component,
    MyComponents
  ]
})
export class View2Module { }
