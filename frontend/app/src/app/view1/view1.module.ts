import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { View1Component } from './view1.component';
import { MyComponents } from '../components/MyComponents/my-components.module';

@NgModule({
  declarations: [View1Component],
  imports: [
    CommonModule,
    MyComponents
  ],
  exports: [
    View1Component,
    MyComponents
  ]
})
export class View1Module { }
