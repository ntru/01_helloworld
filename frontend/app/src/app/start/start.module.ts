import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartComponent } from './start.component';
import { MyComponents } from '../components/MyComponents/my-components.module';


@NgModule({
  declarations: [StartComponent],
  imports: [
    CommonModule,
    MyComponents
  ],
  exports: [
    StartComponent,
    MyComponents
  ]
})
export class StartModule { }
